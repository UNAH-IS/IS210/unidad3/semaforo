﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Semaforo
{
    public partial class Form1 : Form
    {
        Bitmap[] semaforo;
        int posicionActual;
        int tiempoFaltante;

        public Form1()
        {
            InitializeComponent();

            Habilitar(false);
            semaforo = new Bitmap[3];
            this.posicionActual = 0;
            this.tiempoFaltante = 0;

            semaforo[0] = new Bitmap(Recursos.semaforo_rojo);
            semaforo[1] = new Bitmap(Recursos.semaforo_naranja);
            semaforo[2] = new Bitmap(Recursos.semaforo_verde);
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown1.Value = trackBar1.Value;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            trackBar1.Value = (int)numericUpDown1.Value;
        }

        private void ColorearBotones(Button btn, Color nuevoColor)
        {
            if (btn.Enabled)
            {
                btn.BackColor = nuevoColor;
            } else
            {
                btn.BackColor = DefaultBackColor;
            }
        }
        private void Habilitar(bool estado)
        {
            int milisegundos = trackBar1.Value * 1000;

            if (milisegundos == 0)
            {
                return;
            }

            if (estado)
            {
                timer1.Interval = milisegundos;
            }

            btnDetener.Enabled = estado;
            timer1.Enabled = estado;
            timer2.Enabled = estado;
            btnIniciar.Enabled = !estado;
            trackBar1.Enabled = !estado;
            numericUpDown1.Enabled = !estado;

            ColorearBotones(btnIniciar, Color.LightGreen);
            ColorearBotones(btnDetener, Color.Red);
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            this.posicionActual = 0;
            Habilitar(true);
            AnimarSemaforo();
        }

        private void btnDetener_Click(object sender, EventArgs e)
        {
            Habilitar(false);
        }

        private void AnimarSemaforo()
        {
            if (this.posicionActual == 2)
            {
                this.posicionActual = 0;
            }
            else
            {
                this.posicionActual++;
            }

            pictureBox1.Image = semaforo[this.posicionActual];
            this.tiempoFaltante = trackBar1.Value;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            AnimarSemaforo();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            this.label2.Text = this.tiempoFaltante.ToString();
            this.tiempoFaltante--;
        }
    }
}
